import client from 'webpack-theme-color-replacer/client'
const forElementUI = require('webpack-theme-color-replacer/forElementUI')

export default {
  changeColor (newColor) {
    const options = {
      newColors: forElementUI.getElementUISeries(newColor).concat('#fff'), // new colors array, one-to-one corresponde with `matchColors`
      changeUrl (cssUrl) {
        return `/${cssUrl}` // while router is not `hash` mode, it needs absolute path
      }
    }
    return client.changer.changeColor(options, Promise)
  }
}
