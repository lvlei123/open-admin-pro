// eslint-disable-next-line
import { UserLayout, BasicLayout, RouteView, BlankLayout, PageView } from '@/layouts'
import { bxAnaalyse } from '@/libs/icons'

// 默认路由
export const defaultRouterMap = [
  {
    path: '/',
    name: 'index',
    component: BasicLayout,
    redirect: '/home',
    children: [
      {
        name: 'home',
        path: '/home',
        component: RouteView,
        hidden: true,
        redirect: '/home/index',
        children: [
          {
            path: '/home/index',
            name: 'homeIndex',
            component: () => import('@/views/home'),
            meta: { title: '首页' }
          }
        ]
      },
      {
        path: '/account',
        component: RouteView,
        redirect: '/account/center',
        name: 'account',
        hidden: true,
        meta: { title: '个人页', icon: 'user', keepAlive: true, authorities: [ 'user' ] },
        children: [
          {
            path: '/account/center',
            name: 'center',
            component: () => import('@/views/account/center/Index'),
            meta: { title: '个人中心', keepAlive: true, authorities: [ 'user' ] }
          },
          {
            path: '/account/settings',
            name: 'settings',
            component: () => import('@/views/account/settings/Index'),
            meta: { title: '个人设置', hideHeader: true, authorities: [ 'user' ] },
            redirect: '/account/settings/base',
            hideChildrenInMenu: true,
            children: [
              {
                path: '/account/settings/base',
                name: 'BaseSettings',
                component: () => import('@/views/account/settings/BaseSetting'),
                meta: { title: '基本设置', hidden: true, authorities: [ 'user' ] }
              },
              {
                path: '/account/settings/security',
                name: 'SecuritySettings',
                component: () => import('@/views/account/settings/Security'),
                meta: { title: '安全设置', hidden: true, keepAlive: true, authorities: [ 'user' ] }
              },
              {
                path: '/account/settings/custom',
                name: 'CustomSettings',
                component: () => import('@/views/account/settings/Custom'),
                meta: { title: '个性化设置', hidden: true, keepAlive: true, authorities: [ 'user' ] }
              },
              {
                path: '/account/settings/binding',
                name: 'BindingSettings',
                component: () => import('@/views/account/settings/Binding'),
                meta: { title: '账户绑定', hidden: true, keepAlive: true, authorities: [ 'user' ] }
              },
              {
                path: '/account/settings/notification',
                name: 'NotificationSettings',
                component: () => import('@/views/account/settings/Notification'),
                meta: { title: '新消息通知', hidden: true, keepAlive: true, authorities: [ 'user' ] }
              }
            ]
          }
        ]
      }
    ]
  }
]

export const demoRouterMap = [
  // demo示例,发布时注释掉
  // forms
  {
    path: '/form',
    redirect: '/form/base-form',
    component: PageView,
    meta: { title: '表单页', icon: 'form', authorities: [ 'form' ] },
    children: [
      {
        path: '/form/base-form',
        name: 'BaseForm',
        component: () => import('@/views/demo/form/BasicForm'),
        meta: { title: '基础表单', keepAlive: true, authorities: [ 'form' ] }
      },
      {
        path: '/form/step-form',
        name: 'StepForm',
        component: () => import('@/views/demo/form/stepForm/StepForm'),
        meta: { title: '分步表单', keepAlive: true, authorities: [ 'form' ] }
      },
      {
        path: '/form/advanced-form',
        name: 'AdvanceForm',
        component: () => import('@/views/demo/form/advancedForm/AdvancedForm'),
        meta: { title: '高级表单', keepAlive: true, authorities: [ 'form' ] }
      }
    ]
  },

  // list
  {
    path: '/list',
    name: 'list',
    component: PageView,
    redirect: '/list/query-list',
    meta: { title: '列表页', icon: 'table', authorities: [ 'table' ] },
    children: [
      {
        path: '/list/query-list',
        name: 'QueryList',
        component: () => import('@/views/demo/list/QueryList'),
        meta: { title: '查询列表', keepAlive: true, authorities: [ 'table' ] }
      }
    ]
  },

  // profile
  {
    path: '/profile',
    name: 'profile',
    component: RouteView,
    redirect: '/profile/basic',
    meta: { title: '详情页', icon: 'profile', authorities: [ 'profile' ] },
    children: [
      {
        path: '/profile/basic',
        name: 'ProfileBasic',
        component: () => import('@/views/demo/profile/basic/Index'),
        meta: { title: '基础详情页', authorities: [ 'profile' ] }
      },
      {
        path: '/profile/advanced',
        name: 'ProfileAdvanced',
        component: () => import('@/views/demo/profile/advanced/Advanced'),
        meta: { title: '高级详情页', authorities: [ 'profile' ] }
      }
    ]
  },

  // result
  {
    path: '/result',
    name: 'result',
    component: PageView,
    redirect: '/result/success',
    meta: { title: '结果页', icon: 'check-circle-o', authorities: [ 'result' ] },
    children: [
      {
        path: '/result/success',
        name: 'ResultSuccess',
        component: () => import(/* webpackChunkName: "result" */ '@/views/demo/result/Success'),
        meta: { title: '成功', keepAlive: false, hiddenHeaderContent: true, authorities: [ 'result' ] }
      },
      {
        path: '/result/fail',
        name: 'ResultFail',
        component: () => import(/* webpackChunkName: "result" */ '@/views/demo/result/Error'),
        meta: { title: '失败', keepAlive: false, hiddenHeaderContent: true, authorities: [ 'result' ] }
      }
    ]
  },

  // Exception
  {
    path: '/exception',
    name: 'exception',
    component: RouteView,
    redirect: '/exception/403',
    meta: { title: '异常页', icon: 'warning', authorities: [ 'exception' ] },
    children: [
      {
        path: '/exception/403',
        name: 'Exception403',
        component: () => import(/* webpackChunkName: "fail" */ '@/views/exception/403'),
        meta: { title: '403', authorities: [ 'exception' ] }
      },
      {
        path: '/exception/404',
        name: 'Exception404',
        component: () => import(/* webpackChunkName: "fail" */ '@/views/exception/404'),
        meta: { title: '404', authorities: [ 'exception' ] }
      },
      {
        path: '/exception/500',
        name: 'Exception500',
        component: () => import(/* webpackChunkName: "fail" */ '@/views/exception/500'),
        meta: { title: '500', authorities: [ 'exception' ] }
      }
    ]
  },
  // other
  {
    path: '/other',
    name: 'otherPage',
    component: PageView,
    meta: { title: '其他组件', icon: 'slack', authorities: [ 'dashboard' ] },
    redirect: '/other/icon-selector',
    children: [
      {
        path: '/other/icon-selector',
        name: 'TestIconSelect',
        component: () => import('@/views/demo/other/IconSelectorView'),
        meta: { title: 'IconSelector', icon: 'tool', keepAlive: true, authorities: [ 'dashboard' ] }
      }
    ]
  }
]

/**
 * 基础路由
 * @type { *[] }
 */
export const constantRouterMap = [
  {
    path: '/user',
    component: UserLayout,
    redirect: '/user/login',
    hidden: true,
    children: [
      {
        path: 'login',
        name: 'login',
        component: () => import(/* webpackChunkName: "user" */ '@/views/user/Login')
      },
      {
        path: 'register',
        name: 'register',
        component: () => import(/* webpackChunkName: "user" */ '@/views/user/Register')
      },
      {
        path: 'register-result',
        name: 'registerResult',
        component: () => import(/* webpackChunkName: "user" */ '@/views/user/RegisterResult')
      }
    ]
  },
  {
    path: '/404',
    component: () => import(/* webpackChunkName: "fail" */ '@/views/exception/404')
  }
]

// 前端未找到页面路由（固定不用改）
export const notFoundRouter =
  {
    path: '*', redirect: '/404', hidden: true
  }
