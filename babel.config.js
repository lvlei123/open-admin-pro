module.exports = {
  presets: [
    '@vue/app',
    [
      '@babel/preset-env',
      {
        'corejs': '3', // 声明corejs版本
        'useBuiltIns': 'entry'
      }
    ]
  ]
}
